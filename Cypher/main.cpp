#include <iostream>
#include <string>
#include <cmath>
using namespace std;


//*******************************************************************
// Definición de la función CCypher01 que cifra la palabra recibida *
// en el parámetro "word", trasladando cada letra hacia la derecha  *
// el número de espacios dados por el valor del parámetro "shift".  *
// La función devuelve la palabra cifrada "cypheredWord", que es    *
// una cadena de caracteres.                                        *
//*******************************************************************


string CCypher01(const string &word, int shift)
{
    string cypheredWord = "";

    for (unsigned int i = 0; i < word.length(); i++)
    {
        char c = toupper(word[i]);
        c = (c - 'A' + shift) % 26 + 'A';
        cypheredWord.push_back(c);
    }
    return cypheredWord;
}


//**********************************************************************
// Definición de la función CCypher02 que cifra la palabra recibida    *
// en el parámetro "word", trasladando cada letra hacia la derecha     *
// el número de espacios dados por el valor del parámetro "shift".     *
// Esta función no devuelve nada pero el parámetro por referencia      *
// "&cypheredWord" permite "devolver" el resultado modificando         *
// directamente el valor del argumento "cypheredText".                 *
//**********************************************************************


void CCypher02(const string &word, int shift, string &cypheredWord)
{
    cypheredWord = "";

    for (unsigned int i = 0; i < word.length(); i++)
    {
        char c = toupper(word[i]);
        c = (c - 'A' + shift) % 26 + 'A';
        cypheredWord.push_back(c);
    }
}


//**********************************************************
// Escribe aquí abajo tu función para decifrar el mensaje. *
//**********************************************************




//***************
// Función main *
//***************


int main()
{
    string plainText = "caballoz";
    string cypheredText;




    return 0;
}

