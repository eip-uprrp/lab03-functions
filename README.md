#Lab. 3: Utilizando funciones en C++

Una buena manera de organizar y estructurar los programas de computadoras es dividiéndolos en partes más pequeñas utilizando funciones. Cada función realiza una tarea específica del problema que estamos resolviendo.

Hemos visto que todos los programas en C++ deben contener la función `main` que es donde comienza el programa. También hemos utilizado las funciones `pow`, `sin` y `cos`  de la biblioteca de matemática `cmath`. Dado que en casi todas las experiencias de laboratorio que siguen estaremos utilizando funciones que ya han sido creadas, necesitamos aprender como trabajar con ellas. Más adelante aprenderemos más detalles sobre funciones.

#Objetivos:

Al finalizar la experiencia de laboratorio de hoy los estudiantes habrán reforzado su conocimiento sobre los elementos escenciales de la definición de una función en C++: tipo, nombre, lista de parámetros y cuerpo de la función. Habrán practicado el invocar funciones ya creadas enviando valores para los parámetros ("pass by value") y referencias para los parámetros ("pass by reference"). También habrán creado una función simple que tiene parámetros que son variables de refefrencia. En adición habrán visto elementos básicos del manejo y operaciones de caracteres.

#Pre-Lab:

Antes de llegar al laboratorio cada estudiante debe:

1. haber repasado 

	a. los elementos básicos de la definición de una función en C++

	b. la manera de invocar funciones en C++

	c. la diferencia entre variables y variables de referencia en los parámetros

	d. como regresar el resultado de una función.

2. haber visto el video sobre el "Ceasar Cypher" del Khan Academy, colocado en <https://www.youtube.com/watch?v=sMOZf4GN3oc>.

3. haber estudiado los conceptos e instrucciones para la sesión de laboratorio.

4. haber tomado el quiz Pre-Lab 3 (recuerda que el quiz Pre-Lab puede tener conceptos explicados en las instrucciones del laboratorio).


#Sesión de laboratorio:

##Funciones

En matemática, una función $f$ es una regla que se usa para asignar a cada elemento $x$ de un conjunto que llamamos *dominio*, uno (y solo un) elemento $y$ de un conjunto que llamamos *campo de valores*. Muchas veces representamos esa regla como una ecuación $y=f(x)$. La variable $x$ es el parámetro de la función y la variable $y$ contendrá el resultado. Una función puede tener más de un parámetro pero solo un resultado. Por ejemplo, podemos tener funciones de la forma $y=f(x_1,x_2)$ en donde hay dos parámetros y para cada par $(a,b)$ que se use como argumento de la función, la función tiene un solo valor de $y=f(a,b)$. El dominio de la función nos dice el tipo de valor que debe tener el parámetro y el campo de valores el tipo de valor que tendrá el resultado.

Las funciones en programación de computadoras son similares. Una función 
tiene una serie de instrucciones que toman los valores asignados a los parámetros y realiza alguna tarea. La única diferencia es que una función en programación puede que no devuelva ningún valor (en este caso la función se declara `void`). Si se va a devolver algún valor, esto lo hace la instrucción `return`. Al igual que en matemática especificamos el dominio y el campo de valores, en programación debemos especificar los tipos de valores que tienen los parámetros y el resultado; esto se hace al declarar la función.

**Encabezado de una función:**

La primera oración de una función se llama el *encabezado* y su estructura es como sigue:

`tipo nombre(tipo parámetro01, ..., tipo parámetro0n)`

Por ejemplo,

`int ejemplo(int var1, float var2, char &var3)`

sería el encabezado de la función llamada `ejemplo`, que devuelve un valor entero. La función recibe como argumentos un valor entero (y guardará una copia en `var1`), un valor "float" (y guardará una copia en `var2`) y la referencia a una variable de tipo  `char` que se guardará en la variable de referencia `var3`. Nota que `var3` tiene un signo de `&` antes del nombre de la variable. Esto indica que `var3` contendrá la referencia a un caracter.

**Invocación**

Si queremos guardar el valor del resultado de la función `ejemplo` en la variable `resultado` (que deberá ser de tipo entero), invocamos la función enviando argumentos de manera similar a:

`resultado=ejemplo(2, 3.5, unCar);`

Nota que no se incluye el tipo de las variables en la invocación. Como en la definición de la función `ejemplo` el tercer parámetro `&var3` es una variable de referencia, lo que se está enviando en el tercer argumento de la invocación es una *referencia* a la variable `unCar`. Los cambios que se hagan en la variable `var3` están cambiando el contenido de la variable `unCar`.

También puedes usar el resultado de la función sin tener que guardarlo en una variable. Por ejemplo puedes imprimirlo:

`cout << "El resultado de la función ejemplo es:" << ejemplo(2, 3.5, unCar);`

o utilizarlo en una expresión aritmética:

`y=3 + ejemplo(2, 3.5, unCar);`

##Funciones que utilizaremos

Uno de los programas que estaremos modificando en la sesión de hoy utiliza las siguientes funciones de `string`, la biblioteca para manejo de cadenas de caracteres:

* `length`: Devuelve el largo de un objeto (variable) tipo `string`; esto es, `length` devuelve el número de caracteres que tiene el "string" que está guardado en el objeto. Se utiliza escribiendo `.length()` después del nombre de la variable.

* `push_back`: Esta función recibe un valor como argumento y lo añade al final de un vector. Se utiliza escribiendo `.push_back(elValor)` después del nombre de la variable. Por ejemplo, para añadir el caracter 'a' a una cadena de caracteres guardada en la variable `cadena`, escribimos `cadena.push_back('a')`.

También utilizaremos la función `toupper`. 

* `toupper`: Esta función recibe como argumento un caracter y devuelve el caracter en mayúscula. Por ejemplo, para cambiar el caracter 'a' a mayúscula se utiliza `toupper('a')`.


##Criptografía y el Cifrado César (Ceasar Cypher)

La *criptografía* es un área que estudia la teoría y los métodos utilizados para proteger información de modo que personas que no estén autorizadas no puedan tener acceso a ella. Un *sistema criptográfico* es un sistema en donde un *cifrador* transforma la información (*mensaje*) en un texto cifrado inentendible para las personas no autorizadas a verlo. Las personas autorizadas a ver el mensaje usan un *descifrador* para transformar el texto cifrado en el mensaje original.

El Cifrado César es una técnica muy simple de cifrado por sustitución. Se dice que el sistema se basa en el sistema utilizado por Julio César, líder militar y político de Roma en años antes de Cristo, para comunicarse con sus generales.

La técnica transforma un mensaje en texto cifrado sustituyendo cada letra del mensaje por la letra que se encuentra a un número dado de posiciones más adelante. Esto puede pensarse como un desplazamiento ("shift") del alfabeto. El diagrama de abajo representa un desplazamiento de 3 espacios. La letra 'B' es sustituida por la letra 'E'.

<div align='center'><img src="http://i.imgur.com/DYSdjlN.png?1" width="400" height="200" alt="New Project" /></div>

Por ejemplo, la palabra "ARROZ" quedaría cifrada como "DUURC". Nota que, con este desplazamiento de 3 espacios, la letra Z es sustituida por la letra 'C'.  Cuando el desplazamiento se pasa de la letra 'Z' comenzamos nuevamente en la letra 'A'. Esto se llama un desplazamiento cíclico. La figura de abajo muestra un desplazamiento cíclico de 8 espacios.

<div align='center'><img src="http://i.imgur.com/MZB8k1j.png?1" width="300" height="300" alt="New Project" /></div>


###Operador módulo

Podemos pensar que a cada letra le asignamos un número comenzando con el 0, que será asignado a la letra 'A'. Asumiendo que usamos un alfabeto con 26 letras (inglés), de la 'A' a la 'Z' tendremos los números del 0 al 25. Para hacer el desplazamiento cíclico, cada vez que nuestro desplazamiento nos dé una letra que corresponda a un número mayor que 25, tomamos el residuo al dividir por 26 y usamos la letra que corresponda a ese residuo. Nota que tomar el residuo al dividir por 26 funciona también para números entre 0 y 25, ?puedes explicar por qué?

El residuo al dividir dos números enteros se puede obtener utilizando el operador de *módulo*: `%`.

Volviendo al ejemplo anterior, a la letra 'Z' le corresponde el número 25; al hacer un desplazamiento de 3 espacios, sumamos 3 a 25 y tomamos el residuo al dividir el resultado por 26: `(25 + 3) % 26`. Esto nos dá 2, que corresponde a la letra 'C'.

Un detalle importante es que en C++ el operador módulo puede devolver resultados negativos. Si estamos usando  `% d` y obtenemos un entero negativo $a$ con $-d+1 \leq a \leq -1$, el equivalente positivo de $a$ será el entero $d-a$.

Por ejemplo, si a la letra 'B' le corresponde el número 1, al desplazarla -3 espacios (3 espacios a la izquierda) obtenemos `(1 - 3) % 26`, que es `-2 % 26`. El el operdor `%` en C++ devolverá -2 que no corresponde a ninguna letra entre 'A' y 'Z'. Por eso consideramos $26-2=24$, que es equivalente a -2 módulo 26. Nota que 24 corresponde a la letra 'Y' y es lo que obtenemos al hacer un desplazamiento cíclico 2 espacios hacia la izquierda. 

###**Ejercicio 1**

En este ejercicio practicarás como invocar funciones cifrando una palabra con  el cifrador César. La palabra solo podrá contener letras y el desplazamiento debe ser un número entre 0 y 25. 

**Instrucciones**

1.	Ve a la pantalla de terminal y escribe el comando `git clone https://bitbucket.org/eip-uprrp/Lab03-Functions.git` para descargar la carpeta `Lab03-Functions` a tu computadora.

2.	Marca doble "click" en el archivo `Cypher.pro` para cargar este proyecto a Qt. 

3. El archivo `main.cpp` (en Sources) es donde estarás cambiando y añadiendo código. Abre ese archivo y estudia la función `CCypher01`. 

En las secciones correspondientes de la [página de Entregas del Lab 3](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6621) indica cuáles son los parámetros de la función y cuáles son sus tipos. 

Para simplificar el programa estamos cambiando todas las letras a mayúsculas. ?Dónde se hace esto en el programa? También, en la explicación del cifrado César, asumimos que el valor de las letras 'A' a la 'Z' va desde 0 hasta 25. Pero en el código ASCII el valor de las letras 'A' a la 'Z' va desde 65 hasta 90, por lo que necesitamos hacer un ajuste en el cómputo. 

Lo siguiente es un pseudo-código para la función `CCypher01`:

```
    Inputs: plaintext, a string d, Caesar Cypher shift amount 
    Output: a Ceasar Cyphered string

    1. cypheredText = ""
    2. for each character c in plaintext:
           c = ascii(c) - ascii('A')  # map c from [A,Z] to [0,25]
           c = ( c + d ) % 26         # shift c by d units
           c = ascii(c) + ascii('A')  # map c from [0,25] to [A,Z]
           cypheredText.append(c) 
    3. return cypheredText 
```


En las secciones correspondientes de la [página de Entregas del Lab 3](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6621) explica en tus propias palabras qué hace la función `CCypher01`.


4. Busca la función `main` del programa. Añade código de modo que invoques la función `CCypher01` dando como argumentos la variable `plainText` y un valor para el desplazamiento. Añade además código de modo que el programa despliegue el mensaje original y el texto cifrado. Escribe las oraciones del código que añadiste en la sección correspondiente de la [página de Entregas del Lab 3](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6621).

5. Ahora estudia la función `CCypher02`. ?En qué se diferencia de la función `CCypher01`? En la función `main`, añade código de modo que invoques la función `CCypher02` enviando como argumento la variable `cypheredText` y luego despliegues el mensaje original y el texto cifrado. Escribe las oraciones del código que añadiste en la sección correspondiente de la [página de Entregas del Lab 3](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6621).

6. Escribe una función `DeCypher` para descifrar mensajes. Asume que el desplazamiento utilizado es menor que 26 y recuerda que hay que tener cuidado con posibles resultados negativos. Prueba tu función cifrando un mensaje con cualquiera de las funciones `CCypher`  y luego descifrándolo invocando `DeCypher` desde la función `main`. Verifica que el texto descifrado sea igual al mensaje original. Escribe el código de tu función `DeCypher` en la sección correspondiente de la [página de Entregas del Lab 3](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6621).

7. Vamos a practicar composición de funciones en C++. Haz una invocación de la función `DeCypher`  compuesta con una de las funciónes  `CCypher` . Nota que las funciones `DeCypher`  y `CCypher` son funciones inversas. Al igual que en la composición de funciones inversas en matemáticas, en C++ el resultado de la composición devuelve el argumento de `CCypher`. Esto es, la composición de estas funciones debe devolver el mensaje original. Escribe el código de la invocación de la composición en la sección correspondiente de la [página de Entregas del Lab 3](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6621).

###**Ejercicio 2**

En la explicación de las funciones vimos que tanto en matemáticas como en programación una función no puede devolver más de un resultado. En este ejercicio haremos una modificación al proyecto `PrettyPlots` para practicar como podemos usar funciones y obtener varios resultados. 

**Instrucciones**

1.  Marca doble "click" en el archivo `prettyPlot.pro` para cargar este proyecto a Qt. 

2. El archivo `main.cpp` (en Sources) es donde estarás cambiando el código. Primero veremos la diferencia entre hacer cambios en variables que reciben valores y hacer cambios en variables de referencia. Estudia la función `ilustracion` y su invocación desde la función `main`. Ejecuta el programa y observa lo que se despliega en la pantalla `Application Output`. En las secciones correspondientes de la [página de Entregas del Lab 3](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6621) indica la diferencia entre las variables `paraValor` y `paraRef` y explica por qué el contenido de `argValor` no cambia, mientras que el contenido de `argRef` cambia de 0 a 1.

3. Ahora crearemos una función para calcular las coordenadas de los puntos de la gráfica de la mariposa que vimos en `PrettyPlots`. Busca la sección de la función `main` en donde se calculan las coordenadas de los puntos de la gráfica de la mariposa. 

3. Escribe una función `mariposa` que no devuelva valor pero que  calcule las coordenadas de los puntos de la mariposa y, usando parámetros por referencia, permita que la función `main` tenga esos valores en las variables `x` , `y`. Para que esto pase la función `mariposa` debe ser invocada desde la función `main`. Recuerda borrar la sección de `main` en donde se calculaban los puntos orginalmente. Escribe la función `mariposa` y su invocación en la sección correspondiente de la [página de Entregas del Lab 3](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6621).

